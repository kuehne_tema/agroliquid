package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.tbs.generic.vansales.Model.InspectionDO;
import com.tbs.generic.vansales.Model.InspectionMainDO;
import com.tbs.generic.vansales.Model.SerialListDO;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.ProgressTask;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.util.ArrayList;


public class InspectionQuestionsListRequest extends AsyncTask<String, Void, Boolean> {
    private Context mContext;
    InspectionMainDO inspectionMainDO;
    String equipment;
    int type,lineNumber;
    InspectionDO inspectionDO;

    String contractNumber, preparationNumber;

    public InspectionQuestionsListRequest(Context mContext,

                                          String equipment,
                                          String contract,
                                          String preparation,
                                          int line,
                                          int type) {
        this.mContext = mContext;
        this.type = type;
        this.equipment = equipment;
        this.contractNumber = contract;
        this.preparationNumber = preparation;
        this.lineNumber = line;

    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        public void onCompleted(boolean isError, InspectionMainDO contractMainDo);
    }

    public boolean runRequest() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("I_YITM", equipment);



        } catch (Exception e) {
            return false;
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();

        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.QUESTIONS, jsonObject);

        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ProgressTask.getInstance().showProgress(mContext, false, "");
    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();
            inspectionMainDO = new InspectionMainDO();


            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        inspectionMainDO.inspectionDOS = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                       inspectionDO  = new InspectionDO();

                    }

                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("O_YQSTNCOD")) {
                            if(!text.isEmpty()){
                                inspectionDO.question = text;

                            }


                        }
                        if (attribute.equalsIgnoreCase("O_YQSTNDES")) {
                            if(!text.isEmpty()) {

                                inspectionDO.questionDes = text;
                            }

                        }
                        text = "";

                    }

                    if (endTag.equalsIgnoreCase("GRP")) {
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        inspectionMainDO.inspectionDOS.add(inspectionDO);
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        ProgressTask.getInstance().closeProgress();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, inspectionMainDO);
        }
    }
}