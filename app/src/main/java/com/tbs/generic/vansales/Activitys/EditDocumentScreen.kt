package com.tbs.generic.vansales.Activitys

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.*
import android.os.Build
import android.os.Environment
import android.text.TextUtils
import android.util.AttributeSet
import android.util.Base64
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.widget.NestedScrollView
import com.bumptech.glide.Glide
import com.darsh.multipleimageselect.activities.AlbumSelectActivity
import com.darsh.multipleimageselect.models.Image
import com.tbs.generic.vansales.Adapters.FilesPreviewAdapter
import com.tbs.generic.vansales.Model.ActiveDeliveryMainDO
import com.tbs.generic.vansales.Model.FileDetails
import com.tbs.generic.vansales.Model.UpdateDocumentDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.UpdateDocumentRequest
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.listeners.StringListner
import com.tbs.generic.vansales.utils.Util
import kotlinx.android.synthetic.main.activity_digital_signature.nested_scroll
import kotlinx.android.synthetic.main.edit_document.*
import kotlinx.android.synthetic.main.include_image_caputure.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.util.*

/**
 *Created by VenuAppasani on 22-12-2018.
 *Copyright (C) 2018 TBS - All Rights Reserved
 */
class EditDocumentScreen : BaseActivity(), View.OnClickListener {
    private lateinit var filesPreviewAdapter: FilesPreviewAdapter
    lateinit var mClear: Button
    lateinit var mGetSign: Button
    lateinit var file: File
    lateinit var mContent: LinearLayout
    lateinit var view: View
    lateinit var mSignature: signature
    var bitmap: Bitmap? = null
    lateinit var savedPath: String
    lateinit var encodedImage: String
    lateinit var updateDocumentDO: UpdateDocumentDO
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var tvShipmentNumber: TextView
    var DIRECTORY = Environment.getExternalStorageDirectory().getPath() + "/updateUserSignature/"
    var pic_name = /*SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())*/"updatesignature"
    var StoredPath = DIRECTORY + pic_name + ".png"
    private var fileDetailsList: ArrayList<FileDetails>? = null
    lateinit var activeDocumentSavedDo: ActiveDeliveryMainDO
    override fun initialize() {
        var llCategories = getLayoutInflater().inflate(R.layout.edit_document, null) as NestedScrollView
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        initializeControls()
        disableMenuWithBackButton()
//        Util.getActionBarView(supportActionBar)
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {

            finish()
//            showToast(getString(R.string.please_save_Sig))
        }
//        tv_title.setText(getString(R.string.add_document_details));
        activeDocumentSavedDo = StorageManager.getInstance(this).getUpdateActiveDeliveryMainDo(this)
        if (activeDocumentSavedDo.doctype == 1 || activeDocumentSavedDo.doctype == 4) {
            llSignatureScreen.visibility = View.VISIBLE
            mClear.visibility = View.VISIBLE
        } else {
            llSignatureScreen.visibility = View.GONE
            mClear.visibility = View.GONE
        }

        updateDocumentDO = StorageManager.getInstance(this).getUpdateData(this);
//        updateDocumentDO.addNotes=activeDocumentSavedDo.notes
//        updateDocumentDO.signatureEncode=activeDocumentSavedDo.signature
//        StorageManager.getInstance(this).saveUpdateData(this,updateDocumentDO)


        Log.d("iuasndnaskdf->", updateDocumentDO.signatureEncode + "--" + updateDocumentDO.capturedImagesList!!.length)

        imagesSetUp()

        if (!TextUtils.isEmpty(updateDocumentDO.signatureEncode)) {
            mContent.visibility = View.GONE
            imv_signature.visibility = View.VISIBLE

            val imageByteArray: ByteArray = Base64.decode(updateDocumentDO.signatureEncode, Base64.DEFAULT)
            Log.d("file_type", "base64")
            var imageWd = resources.getDimension(R.dimen._40sdp).toInt()
            Glide.with(this)
                    .load(imageByteArray)
                    .centerCrop()
                    .override(imageWd, imageWd)
                    .into(imv_signature)
        } else {
            mContent.visibility = View.VISIBLE
            imv_signature.visibility = View.GONE
        }
        llUpdatenoteFab.setOnClickListener {
            val intent = Intent(this, UpdatePODNotesActivity::class.java)
            startActivity(intent)
        }
        btnAddImages.setOnClickListener {
            Util.preventTwoClick(it)
            val intent = Intent(this, AlbumSelectActivity::class.java)
            intent.putExtra(com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_LIMIT, 1)
            if (fileDetailsList!!.size > 0 && fileDetailsList!!.size <= 4) {
                intent.putExtra(com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_LIMIT, 4 - fileDetailsList!!.size)
            } else {
                intent.putExtra(com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_LIMIT, 4)
            }
            intent.putExtra(com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_FROM, true)
            startActivityForResult(intent, 888)
        }
        tvShipmentNumber.setText(getString(R.string.document_number) + " : " + activeDocumentSavedDo.shipmentNumber)

    }

    override fun initializeControls() {
        mContent = findViewById<View>(R.id.canvasLayout) as LinearLayout
        mSignature = signature(applicationContext, null)
        mSignature.setBackgroundColor(Color.WHITE)
        recycleview = findViewById<View>(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        recycleview.setLayoutManager(androidx.recyclerview.widget.LinearLayoutManager(this))
        mContent.addView(mSignature, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        mClear = findViewById<View>(R.id.clear) as Button
        mGetSign = findViewById<View>(R.id.getsign) as Button
        tvShipmentNumber = findViewById<View>(R.id.tvShipmentNumber) as TextView

        tvScreenTitle.setText(getString(R.string.edit_document))
        view = mContent
        mGetSign.setOnClickListener(this@EditDocumentScreen)
        mClear.setOnClickListener(this@EditDocumentScreen)

        // Method to create Directory, if the Directory doesn't exists
        file = File(DIRECTORY)
        if (!file.exists()) {
            file.mkdir()
        }


    }


    override fun onClick(v: View?) {
        Util.preventTwoClick(v)
        // TODO Auto-generated method stub
        if (v === mClear) {
            mSignature.clear()

        } else if (v === mGetSign) {


            if (Build.VERSION.SDK_INT >= 23) {
                if (isStoragePermissionGranted()) {

                    showAppCompatAlert("", getString(R.string.are_update_document), getString(R.string.ok), getString(R.string.cancel), getString(R.string.success), true)

                }
            } else {

                showAppCompatAlert("", getString(R.string.are_update_document), getString(R.string.ok), getString(R.string.cancel), getString(R.string.success), true)

            }


        }
    }

    override fun onButtonYesClick(from: String) {
        if (getString(R.string.success).equals(from, ignoreCase = true)) {
            view.setDrawingCacheEnabled(true)
            mSignature.save(view, StoredPath)
        }
    }

    fun isStoragePermissionGranted(): Boolean {
        if (Build.VERSION.SDK_INT >= 23) {
            if (applicationContext.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return true
            } else {
                ActivityCompat.requestPermissions(this, arrayOf<String>(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
                return false
            }
        } else {
            return true
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            view.setDrawingCacheEnabled(true)
            mSignature.save(view, StoredPath)
//            Toast.makeText(applicationContext, "Successfully Saved", Toast.LENGTH_SHORT).show()
            // Calling the same class
            recreate()
        } else {
            Toast.makeText(this, getString(R.string.the_app_not_allowed), Toast.LENGTH_LONG).show()
        }
    }

    inner class signature(context: Context, attrs: AttributeSet?) : View(context, attrs) {
        private val paint = Paint()
        private val path = Path()

        private var lastTouchX: Float = 0.toFloat()
        private var lastTouchY: Float = 0.toFloat()
        private val dirtyRect = RectF()

        init {
            paint.setAntiAlias(true)
            paint.setColor(Color.BLACK)
            paint.setStyle(Paint.Style.STROKE)
            paint.setStrokeJoin(Paint.Join.ROUND)
            paint.setStrokeWidth(STROKE_WIDTH)
        }

        @SuppressLint("WrongThread")
        fun save(v: View, StoredPath: String) {
            if (activeDocumentSavedDo.doctype == 1 || activeDocumentSavedDo.doctype == 4) {
                if (bitmap == null && mContent.width != 0 && mContent.height !=0) {
                    bitmap = Bitmap.createBitmap(mContent.width, mContent.height, Bitmap.Config.RGB_565)
                }else{
                    updatedelivery()
                }
                val canvas = bitmap?.let { Canvas(it) }
                try {
                    val mFileOutStream = FileOutputStream(StoredPath)
                    v.draw(canvas)
                    mFileOutStream.flush()
                    mFileOutStream.close()
                    val bos = ByteArrayOutputStream();
                    bitmap!!.compress(Bitmap.CompressFormat.JPEG, 2, bos);
                    val bitmapdata = bos.toByteArray();
                    Log.e("Signature Size : ", "Size : " + bitmapdata)
                    encodedImage = android.util.Base64.encodeToString(bitmapdata, android.util.Base64.DEFAULT)
                    updateDocumentDO = StorageManager.getInstance(this@EditDocumentScreen).getUpdateData(this@EditDocumentScreen);

                    updateDocumentDO.signatureEncode = encodedImage;
                    savedPath = StoredPath
                    StorageManager.getInstance(context).saveUpdateData(this@EditDocumentScreen, updateDocumentDO)
                    if (Util.isNetworkAvailable(context)) {
                        if (!path.isEmpty) {
                            updatedelivery()
                        } else {
                            showToast(context.getString(R.string.please_do_sig))
                        }


                    } else {
                        showAppCompatAlert(getString(R.string.alert), getString(R.string.please_check_internet), getString(R.string.ok), "", getString(R.string.failure), false)

                    }


                } catch (e: Exception) {
                    Log.v("log_tag", e.toString())
                }

            } else {
                updatedelivery()
            }

        }

        fun clear() {
            path.reset()
            invalidate()
            mContent.visibility = View.VISIBLE
            imv_signature.visibility = View.GONE
        }

        fun scaleDown(realImage: Bitmap, maxImageSize: Float,
                      filter: Boolean): Bitmap {
            val ratio = Math.min(
                    maxImageSize.toFloat() / realImage.getWidth(),
                    maxImageSize.toFloat() / realImage.getHeight())
            val width = Math.round(ratio.toFloat() * realImage.getWidth())
            val height = Math.round(ratio.toFloat() * realImage.getHeight())
            val newBitmap = Bitmap.createScaledBitmap(realImage, width,
                    height, filter)
            return newBitmap
        }

        protected override fun onDraw(canvas: Canvas) {
            var BitmapSize = 40
            nested_scroll.requestDisallowInterceptTouchEvent(true);
//            val bitmapiamge = BitmapFactory.decodeResource(resources, R.drawable.gmaps)
//            val width = bitmapiamge.getWidth() + BitmapSize * 2
//            val height = bitmapiamge.getHeight() + BitmapSize * 2
//
//            val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565)
//            val canvas = Canvas(bitmap)
//            canvas.drawColor(Color.GREEN)
//            canvas.drawBitmap(bitmapiamge, BitmapSize.toFloat(), BitmapSize.toFloat(), null)

            canvas.drawPath(path, paint)
        }

        override fun onTouchEvent(event: MotionEvent): Boolean {
            val eventX = event.x
            val eventY = event.y

            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    path.moveTo(eventX, eventY)
                    lastTouchX = eventX
                    lastTouchY = eventY
                    return true
                }

                MotionEvent.ACTION_MOVE,

                MotionEvent.ACTION_UP -> {

                    resetDirtyRect(eventX, eventY)
                    val historySize = event.historySize
                    for (i in 0 until historySize) {
                        val historicalX = event.getHistoricalX(i)
                        val historicalY = event.getHistoricalY(i)
                        expandDirtyRect(historicalX, historicalY)
                        path.lineTo(historicalX, historicalY)
                    }
                    path.lineTo(eventX, eventY)
                }

                else -> {
                    debug("Ignored touch event: " + event.toString())
                    return false
                }
            }

            invalidate((dirtyRect.left - HALF_STROKE_WIDTH).toInt(),
                    (dirtyRect.top - HALF_STROKE_WIDTH).toInt(),
                    (dirtyRect.right + HALF_STROKE_WIDTH).toInt(),
                    (dirtyRect.bottom + HALF_STROKE_WIDTH).toInt())

            lastTouchX = eventX
            lastTouchY = eventY

            return true
        }

        private fun debug(string: String) {

            Log.v("log_tag", string)

        }

        private fun expandDirtyRect(historicalX: Float, historicalY: Float) {
            if (historicalX < dirtyRect.left) {
                dirtyRect.left = historicalX
            } else if (historicalX > dirtyRect.right) {
                dirtyRect.right = historicalX
            }

            if (historicalY < dirtyRect.top) {
                dirtyRect.top = historicalY
            } else if (historicalY > dirtyRect.bottom) {
                dirtyRect.bottom = historicalY
            }
        }

        private fun resetDirtyRect(eventX: Float, eventY: Float) {
            dirtyRect.left = Math.min(lastTouchX, eventX)
            dirtyRect.right = Math.max(lastTouchX, eventX)
            dirtyRect.top = Math.min(lastTouchY, eventY)
            dirtyRect.bottom = Math.max(lastTouchY, eventY)
        }

    }

    companion object {
        private val STROKE_WIDTH = 5f
        private val HALF_STROKE_WIDTH = STROKE_WIDTH / 2
    }

    private fun setUpImagesAs64Bit() {

        for (list in this.fileDetailsList!!) {
            try {
//                val fis = FileInputStream(File(list.fileImage))
//                val bitmap = BitmapFactory.decodeStream(fis)
//                val baos = ByteArrayOutputStream()
//                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
//                val bytes = baos.toByteArray()
//                val encImage = Base64.encodeToString(bytes, Base64.DEFAULT)
//                LogUtils.debug("PIC-->", encImage)

                updateDocumentDO.capturedImagesListBulk!!.add(list.fileImage)

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        StorageManager.getInstance(this).saveUpdateData(this@EditDocumentScreen, updateDocumentDO)

    }


    override fun onBackPressed() {
        finish()
    }

    private fun updatedelivery() {

        setUpImagesAs64Bit()
        updateDocumentDO = StorageManager.getInstance(this).getUpdateData(this);

        val driverListRequest = UpdateDocumentRequest(updateDocumentDO.addNotes, activeDocumentSavedDo.shipmentNumber, this)
        driverListRequest.setOnResultListener { isError, validateDo, msg ->
            hideLoader()
            if (isError) {
                hideLoader()

                if (msg.isNotEmpty()) {
                    showAppCompatAlert(getString(R.string.error), msg, getString(R.string.ok), "", getString(R.string.failure), false)

                } else {
                    showAppCompatAlert(getString(R.string.error), getString(R.string.server_error), getString(R.string.ok), "", getString(R.string.failure), false)


                }
            } else {
                if (validateDo.flag == 2) {
                    showToast(getString(R.string.updated_successfully))
                    finish()
                } else {
                    showToast(getString(R.string.update_failed))

                }

            }
            StorageManager.getInstance(this).deleteUpdateData(this@EditDocumentScreen)

        }
        driverListRequest.execute()
    }

    private fun imagesSetUp() {


        fileDetailsList = ArrayList()
        filesPreviewAdapter = FilesPreviewAdapter(this, fileDetailsList, StringListner {

            if (fileDetailsList!!.size < 4) {
                btnAddImages.visibility = View.VISIBLE

            } else {
                btnAddImages.visibility = View.GONE
            }

            if (fileDetailsList!!.size >= 1) {
                recycleviewImages.visibility = View.VISIBLE
            } else {
                recycleviewImages.visibility = View.GONE
            }
        })
        recycleviewImages.adapter = filesPreviewAdapter

        if (activeDocumentSavedDo.imageDOS != null) {
            Log.d("image-->", activeDocumentSavedDo.imageDOS.size.toString() + "----" + activeDocumentSavedDo.imageDOS)
            var i = 0
            val l = activeDocumentSavedDo.imageDOS.size
            while (i < l) {
                val fileDetails = FileDetails()
                fileDetails.setFileImg(activeDocumentSavedDo.imageDOS[i].photoImage)
                fileDetails.isFileSource = true;

                fileDetailsList?.add(i, fileDetails)
                i++
            }
            filesPreviewAdapter.notifyDataSetChanged()

            if (fileDetailsList!!.size < 4) {
                btnAddImages.visibility = View.VISIBLE
            } else {
                btnAddImages.visibility = View.GONE
            }

            if (fileDetailsList!!.size >= 1) {
                recycleviewImages.visibility = View.VISIBLE
            } else {
                recycleviewImages.visibility = View.GONE
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == 888) {
            val images = data?.getParcelableArrayListExtra<Image>(com.darsh.multipleimageselect.helpers.Constants.INTENT_EXTRA_IMAGES)

            if (images != null) {
                Log.d("image-->", images.size.toString() + "----" + images)
                var i = 0
                val l = images.size
                while (i < l) {
                    val file1 = File(images[i].path)
                    val fileDetails = FileDetails()
                    fileDetails.fileName = file1.name
                    fileDetails.filePath = file1.absolutePath
                    var bmImg = BitmapFactory.decodeFile(file1.absolutePath);

                    val bos = ByteArrayOutputStream();
                    bmImg!!.compress(Bitmap.CompressFormat.JPEG, 2, bos);
                    val bitmapdata = bos.toByteArray();
                    Log.e("Signature Size : ", "Size : " + bitmapdata)
                    var img = android.util.Base64.encodeToString(bitmapdata, android.util.Base64.DEFAULT)
                    fileDetails.setFileImg(img)

                    fileDetailsList?.add(0, fileDetails)
                    i++
                }
                filesPreviewAdapter.notifyDataSetChanged()

                if (fileDetailsList!!.size < 4) {
                    btnAddImages.visibility = View.VISIBLE
                } else {
                    btnAddImages.visibility = View.GONE
                }

                if (fileDetailsList!!.size >= 1) {
                    recycleviewImages.visibility = View.VISIBLE
                } else {
                    recycleviewImages.visibility = View.GONE
                }
            }
        }
    }

    override fun onResume() {

        super.onResume()
    }

}


