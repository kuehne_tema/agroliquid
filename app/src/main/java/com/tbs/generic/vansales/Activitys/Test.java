//package com.tbs.generic.generic.Activitys;
//
//import java.util.List;
//import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
//// classes needed to initialize map
//import com.mapbox.mapboxsdk.Mapbox;
//import com.mapbox.mapboxsdk.maps.MapView;
//// classes needed to add the location component
//import com.mapbox.geojson.Point;
//import com.mapbox.mapboxsdk.maps.MapboxMap;
//import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
//import android.location.Location;
//import android.view.ViewGroup;
//import android.widget.LinearLayout;
//import android.widget.RelativeLayout;
//import android.widget.Toast;
//import com.mapbox.mapboxsdk.geometry.LatLng;
//import android.support.annotation.NonNull;
//import com.mapbox.mapboxsdk.location.LocationComponent;
//import com.mapbox.mapboxsdk.location.modes.CameraMode;
//import com.mapbox.services.android.navigation.ui.v5.NavigationLauncherOptions;
//import com.mapbox.android.core.permissions.PermissionsListener;
//import com.mapbox.android.core.permissions.PermissionsManager;
//// classes needed to add a marker
//import com.mapbox.mapboxsdk.annotations.Marker;
//import com.mapbox.mapboxsdk.annotations.MarkerOptions;
//// classes to calculate a route
//import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute;
//import com.mapbox.services.android.navigation.v5.navigation.NavigationRoute;
//import com.mapbox.api.directions.v5.models.DirectionsResponse;
//import com.mapbox.api.directions.v5.models.DirectionsRoute;
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//import android.util.Log;
//// classes needed to launch navigation UI
//import android.view.View;
//import android.widget.Button;
//import com.mapbox.services.android.navigation.ui.v5.NavigationLauncher;
//import com.tbs.generic.generic.R;
//import com.tbs.generic.generic.utils.PreferenceUtils;
//
//public class Test extends BaseActivity implements OnMapReadyCallback, MapboxMap.OnMapClickListener, PermissionsListener {
//    private MapView mapView;
//    // variables for adding location layer
//    private MapboxMap mapboxMap;
//    private PermissionsManager permissionsManager;
//    private Location originLocation;
//    // variables for adding a marker
//    private Marker destinationMarker;
//    private LatLng originCoord;
//    private LatLng destinationCoord;
//    // variables for calculating and drawing a route
//    private Point originPosition;
//    private Point destinationPosition;
//    private DirectionsRoute currentRoute;
//    private static final String TAG = "DirectionsActivity";
//    private NavigationMapRoute navigationMapRoute;
//    private Button button;
//
//
//    @Override
//    public void onMapReady(MapboxMap mapboxMap) {
//        this.mapboxMap = mapboxMap;
//        Double lat = Double.valueOf(preferenceUtils.getStringFromPreference(PreferenceUtils.LATTITUDE, ""));
//        Double lng = Double.valueOf(preferenceUtils.getStringFromPreference(PreferenceUtils.LATTITUDE, ""));
//
//        enableLocationComponent();
//        originCoord = new LatLng(17.4483, 78.3915);
//        mapboxMap.addOnMapClickListener(this);
//       if (destinationMarker != null) {
//            mapboxMap.removeMarker(destinationMarker);
//        }
//        destinationCoord = new LatLng(lat,lng);
//        destinationMarker = mapboxMap.addMarker(new MarkerOptions()
//                .position(destinationCoord)
//        );
//        destinationPosition = Point.fromLngLat(destinationCoord.getLongitude(), destinationCoord.getLatitude());
//        originPosition = Point.fromLngLat(originCoord.getLongitude(), originCoord.getLatitude());
//        getRoute(originPosition, destinationPosition);
////
////
//    }
//
//    @Override
//    public void onMapClick(@NonNull LatLng point){
//        if (destinationMarker != null) {
//            mapboxMap.removeMarker(destinationMarker);
//        }
//        destinationCoord = point;
//        destinationMarker = mapboxMap.addMarker(new MarkerOptions()
//                .position(destinationCoord)
//        );
//        destinationPosition = Point.fromLngLat(destinationCoord.getLongitude(), destinationCoord.getLatitude());
//        originPosition = Point.fromLngLat(originCoord.getLongitude(), originCoord.getLatitude());
//        getRoute(originPosition, destinationPosition);
//
//
//    }
//
//    private void getRoute(Point origin, Point destination) {
//        NavigationRoute.builder(this)
//                .accessToken(Mapbox.getAccessToken())
//                .origin(origin)
//                .destination(destination)
//                .build()
//                .getRoute(new Callback<DirectionsResponse>() {
//                    @Override
//                    public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
//                        // You can get the generic HTTP info about the response
//                        Log.d(TAG, "Response code: " + response.code());
//                        if (response.body() == null) {
//                            Log.e(TAG, "No routes found, make sure you set the right user and access token.");
//                            return;
//                        } else if (response.body().routes().size() < 1) {
//                            Log.e(TAG, "No routes found");
//                            return;
//                        }
//
//                        currentRoute = response.body().routes().get(0);
//
//                        // Draw the route on the map
//                        if (navigationMapRoute != null) {
//                            navigationMapRoute.removeRoute();
//                        } else {
//                            navigationMapRoute = new NavigationMapRoute(null, mapView, mapboxMap, R.style.NavigationMapRoute);
//                        }
//                        navigationMapRoute.addRoute(currentRoute);
//                        boolean simulateRoute = true;
//
//                        NavigationLauncherOptions options = NavigationLauncherOptions.builder()
//                                .directionsRoute(currentRoute)
//                                .shouldSimulateRoute(simulateRoute)
//                                .build();
//                        // Call this method with Context from within an Activity
//                        NavigationLauncher.startNavigation(Test.this, options);
//                    }
//
//                    @Override
//                    public void onFailure(Call<DirectionsResponse> call, Throwable throwable) {
//                        Log.e(TAG, "Error: " + throwable.getMessage());
//                    }
//                });
//    }
//    @SuppressWarnings( {"MissingPermission"})
//    private void enableLocationComponent() {
//        // Check if permissions are enabled and if not request
//        if (PermissionsManager.areLocationPermissionsGranted(this)) {
//            // Activate the MapboxMap LocationComponent to show user location
//            // Adding in LocationComponentOptions is also an optional parameter
//            LocationComponent locationComponent = mapboxMap.getLocationComponent();
//            locationComponent.activateLocationComponent(this);
//            locationComponent.setLocationComponentEnabled(true);
//            // Set the component's camera mode
//            locationComponent.setCameraMode(CameraMode.TRACKING);
//            originLocation = locationComponent.getLastKnownLocation();
//
//        } else {
//            permissionsManager = new PermissionsManager(this);
//            permissionsManager.requestLocationPermissions(this);
//        }
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
//    }
//
//    @Override
//    public void onExplanationNeeded(List<String> permissionsToExplain) {
//        Toast.makeText(this, "granted", Toast.LENGTH_LONG).show();
//    }
//
//    @Override
//    public void onPermissionResult(boolean granted) {
//        if (granted) {
//            enableLocationComponent();
//        } else {
//            Toast.makeText(this, "not granted", Toast.LENGTH_LONG).show();
//            finish();
//        }
//    }
//
//    @Override
//    protected void onStart() {
//        super.onStart();
//        mapView.onStart();
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        mapView.onResume();
//    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//        mapView.onPause();
//    }
//
//    @Override
//    protected void onStop() {
//        super.onStop();
//        mapView.onStop();
//    }
//
//    @Override
//    protected void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
//        mapView.onSaveInstanceState(outState);
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        mapView.onDestroy();
//    }
//
//    @Override
//    public void onLowMemory() {
//        super.onLowMemory();
//        mapView.onLowMemory();
//    }
//
//    @Override
//    public void initialize() {
//
//        Mapbox.getInstance(this, getString(R.string.access_token));
//        RelativeLayout llOrderDetails = (RelativeLayout) getLayoutInflater().inflate(R.layout.test, null);
//        llBody.addView(llOrderDetails, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
//        mapView = findViewById(R.id.mapView);
////        mapView.onCreate(llOrderDetails);
//        mapView.getMapAsync(this);
//    }
//
//    @Override
//    public void initializeControls() {
//        tvScreenTitle.setText("Navigation");
//
//    }
//}
