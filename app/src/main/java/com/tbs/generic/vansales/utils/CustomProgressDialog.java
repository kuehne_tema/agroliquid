package com.tbs.generic.vansales.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.tbs.generic.vansales.R;


public class CustomProgressDialog extends Dialog {

    Context context;
    ProgressBar progressWheel;
    TextView messageTextView;
    String message;

    public CustomProgressDialog(Context context, String message) {
        super(context);
        // TODO Auto-generated constructor stub
        this.context = context;
        this.message = message;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_progress_dialog);

        this.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        this.setCancelable(false);
        this.setCanceledOnTouchOutside(false);

        progressWheel = findViewById(R.id.custom_progress_dialog_progress_wheel);

        messageTextView = findViewById(R.id.custom_progress_dialog_message);

        if (message != null && message.length() > 0) {
            messageTextView.setText(message);
        } else {
            messageTextView.setVisibility(View.GONE);
        }
    }

}
