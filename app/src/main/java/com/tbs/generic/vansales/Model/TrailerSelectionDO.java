package com.tbs.generic.vansales.Model;

import java.io.Serializable;

public class TrailerSelectionDO implements Serializable {


    public String trailer = "";
    public String trailerDescription = "";
    public String type = "";
    public String link = "";
    public int mass = 0;
    public String massUnit = "";
    public Double axel = 0.0;
    public int quantity = 0;
    public int volume = 0;
    public String volumeUnit = "";
    public boolean isProductAdded;
    public int flag = 0;
    public boolean selected;
    public String loandelivery = "";
    public String poNumber = "";
    public String customer = "";
    public String customerDescription = "";
    public String stocksite = "";
    public int isUpdated=0;

    public String linenumber = "";




}
