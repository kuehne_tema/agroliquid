package com.tbs.generic.vansales.Activitys

import android.app.Activity
import android.content.Intent
import androidx.drawerlayout.widget.DrawerLayout
import android.text.TextUtils
import android.util.Log
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.*
import com.tbs.generic.vansales.Model.ConfigurationDo
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.ConfigurationRequest
import com.tbs.generic.vansales.database.StorageManager
import kotlinx.android.synthetic.main.configuration_layout.*
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.content.ContextCompat.getSystemService
import android.content.Intent.getIntent
import android.content.SharedPreferences
import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import com.tbs.generic.vansales.listeners.ResultListner
import com.tbs.generic.vansales.utils.*
import java.util.*


class ConfigurationActivity : BaseActivity(), AdapterView.OnItemSelectedListener {



    //Get locale method in preferences


    lateinit var llSplash: ScrollView
    private var saveBtn: Button? = null
    private var resetBtn: Button? = null
    internal lateinit var company: String
    internal lateinit var ip: String
    internal lateinit var port: String
    internal lateinit var pool: String
    internal lateinit var dbUser: String
    internal lateinit var dbPass: String
    internal lateinit var rbYes: RadioButton

    lateinit var country: Array<String>
    override fun initialize() {
        llSplash = layoutInflater.inflate(R.layout.configuration_layout, null) as ScrollView
        llBody.addView(llSplash, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        initializeControls()
        location(ResultListner { `object`, isSuccess ->
            if (isSuccess) {

            }
        })

        tvScreenTitle.text = "" + resources.getString(R.string.config_title)


//        ipEditText!!.setText("192.168.1.200")
//        ipEditText!!.setText("183.82.9.23")
//       etPortNumber.setText("8124")
//      etAlias.setText("VANSDEV")
//        userNameEditText!!.setText("RENUKA")
//        etPassword.setText("TU024")

//        ipEditText!!.setText("47.91.104.128")
//       etPortNumber.setText("8124")
//      etAlias.setText("WBGTEST")
//        userNameEditText!!.setText("TEMA")
//        etPassword.setText("Tema@123")

//        ipEditText!!.setText("47.91.105.187")
//       etPortNumber.setText("8124")
//      etAlias.setText("BROSGST")
//        userNameEditText!!.setText("ADMIN")
//        etPassword.setText("admin")

        etCompanyId!!.setText(Constants.CONFIG_COMPANY_ID)
        etIpAddress.setText(Constants.CONFIG_IP_ADDRESS)
        etPortNumber.setText(Constants.CONFIG_PORT_NUMER)
        etAlias.setText(Constants.CONFIG_ALIAS)
        etUserName.setText(Constants.CONFIG_USER_NAME)
        etPassword.setText(Constants.CONFIG_PASSWORD)


        setSupportActionBar(act_toolbar)
        Util.getActionBarView(supportActionBar)

        act_toolbar.setNavigationOnClickListener {
            onBackPressed()
        }

    }

    override fun initializeControls() {
        flToolbar.visibility = View.GONE
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.LEFT)
        country = arrayOf(resources.getString(R.string.english_us), /*resources.getString(R.string.english_uk),*/
                resources.getString(R.string.french)/*, resources.getString(R.string.hindi), resources.getString(R.string.spanish), resources.getString(R.string.chinese)*/)

        //  CustomBranding.setCustomBG(container);
        preferenceUtils = PreferenceUtils(this@ConfigurationActivity)
        resetBtn = findViewById<View>(R.id.btnReset) as Button
        saveBtn = findViewById<View>(R.id.btnSave) as Button
        rbYes = findViewById<View>(R.id.rbYes) as RadioButton

        var uname = preferenceUtils.getStringFromPreference(PreferenceUtils.A_USER_NAME, "")
        var pwd = preferenceUtils.getStringFromPreference(PreferenceUtils.A_PASSWORD, "")
        var ipp = preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "")
        var pport = preferenceUtils.getStringFromPreference(PreferenceUtils.PORT, "")
        var ppool = preferenceUtils.getStringFromPreference(PreferenceUtils.ALIAS, "")

        etIpAddress.setText(ipp)
        etPortNumber.setText(pport)
        etAlias.setText(ppool)
        etUserName.setText(uname)
        etPassword.setText(pwd)

        val spin = findViewById<View>(R.id.spinner) as Spinner
        spin.setOnItemSelectedListener(this@ConfigurationActivity)

        //Creating the ArrayAdapter instance having the country list
        val aa = ArrayAdapter(this, R.layout.language_item, country)
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        //Setting the ArrayAdapter data on the Spinner
        spin.adapter = aa

        resetBtn!!.setOnClickListener {
            Util.preventTwoClick(it)
            etIpAddress.setText("")
            etPortNumber.setText("")
            etAlias.setText("")
            etUserName!!.setText("")
            etPassword.setText("")
        }
        company = etCompanyId!!.text.toString()
        ip = etIpAddress.text.toString()
        port = etPortNumber.text.toString()
        pool = etAlias.text.toString()
        dbUser = etUserName.text.toString()
        dbPass = etPassword.text.toString()
        saveBtn!!.setOnClickListener(OnClickListener {
            if(rbYes.isChecked){
                ServiceURLS.HTTP="https://"
                preferenceUtils.saveString(PreferenceUtils.HTTP, "https://")

            }else{
                ServiceURLS.HTTP="https://"
                preferenceUtils.saveString(PreferenceUtils.HTTP, "https://")

            }
            company = etCompanyId!!.text.toString().trim { it <= ' ' }
            ip = etIpAddress!!.text.toString().trim { it <= ' ' }
            port = etPortNumber.text.toString().trim { it <= ' ' }
            pool = etAlias!!.text.toString().trim { it <= ' ' }
            dbUser = etUserName!!.text.toString().trim { it <= ' ' }
            dbPass = etPassword.text.toString().trim { it <= ' ' }


            if (!validation()) {
                return@OnClickListener
            }
            if (Util.isNetworkAvailable(this)) {
                /* customProgressDialog = CustomProgressDialog(this@ConfigurationActivity, "" + resources.getString(R.string.config_wait))
                 customProgressDialog!!.show()*/
                val configRequest = ConfigurationRequest(this)
                configRequest.setOnResultListener { isError, isSuccess, configurationDo ->
                    println("isError $isError isSuccess $isSuccess")
                    /* if (customProgressDialog != null && customProgressDialog!!.isShowing) {
                         customProgressDialog!!.dismiss()
                     }*/
                    if (isError) {
                        showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.config_auth_error), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_failure), false)
                    } else {
                        if (isSuccess) {

                            changeLocale()
                            preferenceUtils.saveString(PreferenceUtils.COMPANY_ID, company)
                            preferenceUtils.saveString(PreferenceUtils.IP_ADDRESS, ip)
                            preferenceUtils.saveString(PreferenceUtils.PORT, port)
                            preferenceUtils.saveString(PreferenceUtils.ALIAS, pool)
                            preferenceUtils.saveString(PreferenceUtils.A_USER_NAME, dbUser)
                            preferenceUtils.saveString(PreferenceUtils.A_PASSWORD, dbPass)
                            preferenceUtils.saveString(PreferenceUtils.COMPANY_IMG, configurationDo.companyImg)
                            preferenceUtils.saveString(PreferenceUtils.COMPANY_DESCRIPTION, configurationDo.companyDescription)

                            showAppCompatAlert("" + resources.getString(R.string.success), "" + resources.getString(R.string.config_success), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_success), false)

                            // Util.storeAppUrl(ConfigurationActivity.this, ip, port, pool, dbUser, dbPass);

                        } else {
                            showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.config_auth_error), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_failure), false)
                        }
                    }
                }


                configRequest.execute(ip, port, pool, dbUser, dbPass, company)
            } else {
                showAppCompatAlert("" + resources.getString(R.string.alert_message), "" + resources.getString(R.string.internet_connection), "" + resources.getString(R.string.ok), "", "" + resources.getString(R.string.from_failure), false)

            }


        })


    }

    override fun onButtonNoClick(from: String) {

        if (getString(R.string.success).equals(from, ignoreCase = true)) {
            finish()
        }
    }

    override fun onButtonYesClick(from: String) {

        if (getString(R.string.success).equals(from, ignoreCase = true)) {
            val intent = Intent(this@ConfigurationActivity, LoginActivity::class.java)
            startActivity(intent)
            finish()

        } else
            if (getString(R.string.failure).equals(from, ignoreCase = true)) {

            }

    }

    override fun onBackPressed() {
        performBack()
    }

    private fun performBack() {

        var ip = preferenceUtils.getStringFromPreference(PreferenceUtils.IP_ADDRESS, "")
        if (ip.length > 0) {
            val intent = Intent(this@ConfigurationActivity, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }


    }

    private fun validation(): Boolean {
        if (TextUtils.isEmpty(etCompanyId?.text.toString())) {
            txt_company_name.error = getString(R.string.please_enter_company_name)
            Util.scrollToViewPosition(this, etCompanyId)
            return false
        } else {
            txt_company_name.isErrorEnabled = false
        }
        if (TextUtils.isEmpty(etIpAddress?.text.toString())) {
            txt_ip_address.error = getString(R.string.config_enter_ip)
            Util.scrollToViewPosition(this, etIpAddress)
            return false
        } else {
            txt_ip_address.isErrorEnabled = false
        }

        if (TextUtils.isEmpty(etPortNumber?.text.toString())) {
            txt_port.error = getString(R.string.config_port)
            Util.scrollToViewPosition(this, etPortNumber)
            return false
        } else {
            txt_port.isErrorEnabled = false
        }


        if (TextUtils.isEmpty(etAlias?.text.toString())) {
            txt_pool_alias.error = getString(R.string.config_pool)
            Util.scrollToViewPosition(this, etAlias)
            return false
        } else {
            txt_pool_alias.isErrorEnabled = false
        }
        if (TextUtils.isEmpty(etUserName?.text.toString())) {
            txt_user_name.error = getString(R.string.config_user_name)
            Util.scrollToViewPosition(this, etUserName)
            return false
        } else {
            txt_user_name.isErrorEnabled = false
        }
        if (TextUtils.isEmpty(etPassword?.text.toString())) {
            txt_password.error = getString(R.string.config_password)
            Util.scrollToViewPosition(this, etPassword)
            return false
        } else {
            txt_password.isErrorEnabled = false
        }


        return true
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        var lang = "en"//Default Language
        var serviceLang = "ENG"
        if (position == 0) {
            lang = "en"
            txt_company_name.setHint("Company Name")
            txt_ip_address.setHint("IP Address")
            txt_port.setHint("Port")
            txt_pool_alias.setHint("Pool Alias - Folder")
            txt_user_name!!.setHint("Username")
            txt_password.setHint("Password")
            btnReset.setText("Reset")
            btnSave.setText("Configure")
            tv_title.text = "" + resources.getString(R.string.config_title)
        } else if (position == 1) {
            txt_company_name.setHint("Nom de la compagnie")
            txt_ip_address.setHint("ADRESSE IP")
            txt_port.setHint("Port")
            txt_pool_alias.setHint("Alias de pool - Dossier")
            txt_user_name!!.setHint("Nom d\\'utilisateur")
            txt_password.setHint("Mot de passe")
            btnReset.setText("RÉINITIALISER")
            btnSave.setText("Configurer")
            tv_title.text = "Configuration"
            lang = "fr"
            var serviceLang = "FRA"
        }
        saveLocale(lang, serviceLang)//Save the selected locale
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun saveLocale(lang: String, serviceLang: String) {
        preferenceUtils.saveString(PreferenceUtils.Locale_KeyValue, lang)
        AppPrefs.putString(AppPrefs.LANAGUAGE, lang);
        AppPrefs.putString(AppPrefs.SERVICE_LANAGUAGE, serviceLang);
    }
}
