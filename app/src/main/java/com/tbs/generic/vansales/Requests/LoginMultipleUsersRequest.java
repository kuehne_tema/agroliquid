//package com.tbs.generic.vansales.Requests;
//
///**
// * Created by Vijay on 19-05-2016.
// */
//
//import android.content.Context;
//import android.os.AsyncTask;
//
//import com.tbs.generic.vansales.Activitys.BaseActivity;
//import com.tbs.generic.vansales.Model.SuccessDO;
//import com.tbs.generic.vansales.utils.AppPrefs;
//import com.tbs.generic.vansales.utils.Constants;
//import com.tbs.generic.vansales.utils.PreferenceUtils;
//import com.tbs.generic.vansales.utils.ServiceURLS;
//import com.tbs.generic.vansales.utils.WebServiceConstants;
//
//import org.json.JSONObject;
//import org.ksoap2.HeaderProperty;
//import org.ksoap2.SoapEnvelope;
//import org.ksoap2.serialization.SoapObject;
//import org.ksoap2.serialization.SoapSerializationEnvelope;
//import org.ksoap2.transport.HttpTransportSE;
//import org.xmlpull.v1.XmlPullParser;
//import org.xmlpull.v1.XmlPullParserFactory;
//
//import java.io.StringReader;
//import java.net.SocketTimeoutException;
//import java.util.ArrayList;
//import java.util.List;
//
//public class LoginMultipleUsersRequest extends AsyncTask<String, Void, Boolean> {
//
//    private SuccessDO successDO;
//    private Context mContext;
//    private String customerId, driverId, loginDate, loginTime;
//    String username, password, ip, pool, port;
//    PreferenceUtils preferenceUtils;
//    private int flaG;
//
//    public LoginMultipleUsersRequest(String driverID, String customer, String loginDatE, String loginTimE, Context mContext) {
//
//        this.mContext = mContext;
//        this.driverId = driverID;
//        this.customerId = customer;
//        this.loginDate = loginDatE;
//        this.loginTime = loginTimE;
//
//
//    }
//
//    public void setOnResultListener(OnResultListener onResultListener) {
//        this.onResultListener = onResultListener;
//    }
//
//    OnResultListener onResultListener;
//
//    public interface OnResultListener {
//        void onCompleted(boolean isError, SuccessDO createInvoiceDO);
//
//    }
//
//    public boolean runRequest() {
//        // System.out.println("CUSTOMER ID " + customer);
//        String NAMESPACE = "http://www.adonix.com/WSS";
//        String METHOD_NAME = "run";
//        String SOAP_ACTION = "CAdxWebServiceXmlCC";
//        preferenceUtils = new PreferenceUtils(mContext);
//        username = "admin";
//        password = "!V11P5@Tema123*";
//        ip = "183.82.9.25";
//        port = "8124";
//        pool = "SEED";
//        String http = preferenceUtils.getStringFromPreference(PreferenceUtils.HTTP, "");
//        String URL = http + ip + ServiceURLS.COLON + port + ServiceURLS.APP_SUB_URL;
//        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
//        request.addProperty("publicName", WebServiceConstants.MULTIPLE_LOGIN);
//        JSONObject jsonObject = new JSONObject();
//        try {
//
//            jsonObject.put("I_XBPC", customerId);
//            jsonObject.put("I_XDATE", loginDate);
//            jsonObject.put("I_XUSRID", driverId);
//            jsonObject.put("I_XLGNTIM", loginTime);
//
//
//
//        } catch (Exception e) {
//            System.out.println("Exception " + e);
//        }
//        request.addProperty("inputXml", jsonObject.toString());
//
//        SoapObject callcontext = new SoapObject("", "callContext");
//        // Set all input params
//        callcontext.addProperty("codeLang", AppPrefs.getString(AppPrefs.SERVICE_LANAGUAGE, Constants.ENG));
//        callcontext.addProperty("poolAlias", pool);
//        callcontext.addProperty("poolId", "");
//        callcontext.addProperty("codeUser", username);
//        callcontext.addProperty("password", password);
//        callcontext.addProperty("requestConfig", "adxwss.trace.on=off");
//
//        request.addSoapObject(callcontext);
//
//
//        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
//
//        envelope.setOutputSoapObject(request);
//        ((BaseActivity)mContext).allowAllSSL();
//        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
//        androidHttpTransport.debug = true;
//
//        try {
//            List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();
//            headerList.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode((username + ":" + password).getBytes())));
//
//
//            androidHttpTransport.call(SOAP_ACTION, envelope, headerList);
//
//
//            SoapObject response = (SoapObject) envelope.getResponse();
//            String resultXML = (String) response.getProperty("resultXml");
//            if (resultXML != null && resultXML.length() > 0) {
//                return parseXML(resultXML);
//            } else {
//                return false;
//            }
//        } catch (SocketTimeoutException e) {
//            e.printStackTrace();
//            return false;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return false;
//        }
//
//    }
//
//    public boolean parseXML(String xmlString) {
//        System.out.println("xmlString " + xmlString);
//        try {
//            String text = "", attribute = "", startTag = "", endTag = "";
//            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
//            factory.setNamespaceAware(true);
//            XmlPullParser xpp = factory.newPullParser();
//
//            xpp.setInput(new StringReader(xmlString));
//            int eventType = xpp.getEventType();
//
//
//            successDO = new SuccessDO();
//
//            while (eventType != XmlPullParser.END_DOCUMENT) {
//                if (eventType == XmlPullParser.START_TAG) {
//
//                    startTag = xpp.getName();
//                    if (startTag.equalsIgnoreCase("FLD")) {
//                        attribute = xpp.getAttributeValue(null, "NAME");
//                    } else if (startTag.equalsIgnoreCase("GRP")) {
//
//                    } else if (startTag.equalsIgnoreCase("TAB")) {
//                        //    createPaymentDO.customerDetailsDos = new ArrayList<>();
//
//                    } else if (startTag.equalsIgnoreCase("LIN")) {
//                        //      createPaymentDO = new CustomerDetailsDo();
//
//                    }
//                } else if (eventType == XmlPullParser.END_TAG) {
//                    endTag = xpp.getName();
//
//                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {
//
//
//                        if (attribute.equalsIgnoreCase("O_XFLG")) {
//                            if (text.length() > 0) {
//
//                                successDO.flag = Integer.parseInt(text);
//                            }
//
//
//                        }
//                    }
//
//
//                    if (endTag.equalsIgnoreCase("GRP")) {
//                        // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
//                    }
//
//                    if (endTag.equalsIgnoreCase("LIN")) {
//                        // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
//                    }
//
//                } else if (eventType == XmlPullParser.TEXT) {
//                    text = xpp.getText();
//                }
//
//                eventType = xpp.next();
//            }
//            return true;
//        } catch (Exception e) {
//            System.out.println("Exception Parser" + e);
//
//            return false;
//        }
//    }
//
//
//    @Override
//    protected void onPreExecute() {
//        super.onPreExecute();
//        ((BaseActivity) mContext).showLoader();
//
//        // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
//    }
//
//    @Override
//    protected Boolean doInBackground(String... param) {
//        return runRequest();
//    }
//
//    @Override
//    protected void onPostExecute(Boolean result) {
//        super.onPostExecute(result);
//
//        ((BaseActivity) mContext).hideLoader();
//        if (onResultListener != null) {
//            onResultListener.onCompleted(!result, successDO);
//        }
//    }
//}