package com.tbs.generic.vansales.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class PaymentPdfDO implements Serializable {
    public String paymentNumber  = "";
    public String customer            = "";
    public String customerDescription = "";
    public String company = "";
    public String companyCode = "";

    public String country        = "";
    public String landline       = "";
    public String mobile         = "";
    public String fax            = "";
    public String mail           = "";
    public Double amount         = 0.0;
    public String currency       = "";
    public String bank           = "";
    public String chequeNumber   = "";
    public String chequeDate     = "";
    public String invoiceNumber  = "";
    public String flag           = "";
    public String logo = "";
    public String customerStreet      = "";
    public String customerLandMark    = "";
    public String customerTown        = "";
    public String customerCity        = "";
    public String customerPostalCode        = "";

    public String siteDescription = "";
    public String siteAddress1 = "";
    public String siteAddress2 = "";
    public String siteAddress3 = "";
    public String siteCountry = "";
    public String siteCity = "";
    public String sitePostalCode = "";
    public String siteLandLine = "";
    public String siteMobile = "";
    public String siteFax = "";
    public String siteEmail1 = "";
    public String siteEmail2 = "";
    public String siteWebEmail = "";


    public ArrayList<InvoiceNumberDos> invoiceDos = new ArrayList<>();
    public String createUserName="";
    public String createUserID="";
    public String createdTime="";
    public String createdDate="";
    @Override
    public String toString() {
        return "PaymentPdfDO{" +
                "paymentNumber='" + paymentNumber + '\'' +
                ", customer='" + customerDescription + '\'' +
                ", customerDescription='" + customerDescription + '\'' +

                ", country='" + country + '\'' +
                ", landline='" + landline + '\'' +
                ", mobile='" + mobile + '\'' +
                ", fax='" + fax + '\'' +
                ", mail='" + mail + '\'' +
                ", amount=" + amount +
                ", currency='" + currency + '\'' +
                ", bank='" + bank + '\'' +
                ", chequeNumber='" + chequeNumber + '\'' +
                ", chequeDate='" + chequeDate + '\'' +
                ", invoiceNumber='" + invoiceNumber + '\'' +
                ", flag='" + flag + '\'' +
                '}';
    }
}
