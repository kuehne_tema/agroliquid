package com.tbs.generic.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.tbs.generic.vansales.Activitys.ActiveDeliveryActivity;
import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Activitys.Cancel_RescheduleActivity;
import com.tbs.generic.vansales.Activitys.EditDocumentScreen;
import com.tbs.generic.vansales.Activitys.LoginActivity;
import com.tbs.generic.vansales.Activitys.SpotPICKUPSActivity;
import com.tbs.generic.vansales.Activitys.TransactionList;
import com.tbs.generic.vansales.Model.ActiveDeliveryDO;
import com.tbs.generic.vansales.Model.ActiveDeliveryMainDO;
import com.tbs.generic.vansales.Model.PickUpDo;
import com.tbs.generic.vansales.Model.SpotPickUpDo;
import com.tbs.generic.vansales.Model.UpdateDocumentDO;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.Requests.ActiveDeliveryRequest;
import com.tbs.generic.vansales.common.AppConstants;
import com.tbs.generic.vansales.database.StorageManager;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.CalendarUtils;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.Util;

import java.util.ArrayList;

public class SpotPickupAdapter extends RecyclerView.Adapter<SpotPickupAdapter.MyViewHolder> implements LocationListener {
    private static Activity context;

    private ArrayList<SpotPickUpDo> spotPickUpDos;
    Double lat, lng;


    @Override
    public void onLocationChanged(Location location) {
        lat = location.getLatitude();
        lng = location.getLongitude();
        ((BaseActivity) context).preferenceUtils = new PreferenceUtils(context);


    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName, tvAddress, tvTime, tvDistance, tvShipmentNumber;
        private LinearLayout llback;


        public MyViewHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.tvName);
            tvDistance = view.findViewById(R.id.tvDistance);

            tvShipmentNumber = view.findViewById(R.id.tvShipmentNumber);
            tvAddress = view.findViewById(R.id.tvAddress);
            tvTime = view.findViewById(R.id.tvTime);

            llback = view.findViewById(R.id.ll_back);


        }
    }


    public SpotPickupAdapter(Activity context, ArrayList<SpotPickUpDo> pickUpDos) {
        this.context = context;
        this.spotPickUpDos = pickUpDos;

    }


    @SuppressLint("MissingPermission")
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.spot_data, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final SpotPickUpDo productDO = spotPickUpDos.get(position);

        holder.tvName.setText(productDO.customer);
        holder.tvAddress.setText(productDO.receiptNumber);
        holder.tvShipmentNumber.setText("" + productDO.description);
        holder.tvDistance.setText( productDO.distance+" km" );


        String shipmentId = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "");
        holder.llback.setBackground(ContextCompat.getDrawable(context, R.drawable.card_white));
        if (shipmentId.equalsIgnoreCase(productDO.receiptNumber) || (shipmentId.equalsIgnoreCase(""))) {


        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (productDO.version.equals(context.getResources().getString(R.string.app_version))) {
//                    if (productDO.sequenceFlag == 30) {
//                        ((BaseActivity) context).showToast(context.getResources().getString(R.string.force_logout));
//                        preferenceUtils.removeFromPreference(PreferenceUtils.DRIVER_ID);
//                        preferenceUtils.removeFromPreference(PreferenceUtils.LOGIN_SUCCESS);
//                        StorageManager.getInstance(context).deleteSpotSalesCustomerList((TransactionList) context);
//                        AppPrefs.clearPref(context);
//                        clearDataLocalData();
//
//                    } else {
                String shipmentId = ((BaseActivity) context).preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "");
                if (shipmentId.equalsIgnoreCase(productDO.receiptNumber) ||
                        (shipmentId.equalsIgnoreCase(""))) {
                    PreferenceUtils preferenceUtils = ((BaseActivity) context).preferenceUtils;
                    preferenceUtils.saveDouble(PreferenceUtils.LATTITUDE, productDO.lattitude);
                    preferenceUtils.saveDouble(PreferenceUtils.LONGITUDE, productDO.longitude);
                    Intent intent = new Intent(context, ActiveDeliveryActivity.class);
                    intent.putExtra("SHIPMENT_ID", productDO.receiptNumber);
//                  intent.putExtra("Code", productDO.vehicleRoutingId);
                    preferenceUtils.saveString(PreferenceUtils.CUSTOMER, productDO.customer);
                    preferenceUtils.saveString(PreferenceUtils.CUSTOMER_NAME, productDO.description);
                    intent.putExtra("Scheduled_customer", productDO.customer);
                    preferenceUtils.saveInt(PreferenceUtils.PICKUP_DROP_FLAG, 1);
                    preferenceUtils.saveString(PreferenceUtils.SPOT_RECEIPT, productDO.receiptNumber);
                    ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.ProductsType, AppConstants.FixedQuantityProduct);


                    preferenceUtils.saveInt(PreferenceUtils.DOC_TYPE, 2);
                    preferenceUtils.saveString(PreferenceUtils.SHIPMENT, productDO.receiptNumber);
                    ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.ShipmentProductsType, "Cylinder");
                    preferenceUtils.saveString(PreferenceUtils.ShipmentType, context.getResources().getString(R.string.checkin_scheduled));
                    ((BaseActivity) context).startActivityForResult(intent, 19);


                } else {
                    ((BaseActivity) context).showToast(context.getString(R.string.please_process_the_current_shipment));
                }
//                    }


//                } else {
//                    ((TransactionList) context).showAppCompatAlert("" + context.getResources().getString(R.string.login_info), "" + context.getResources().getString(R.string.playstore_messsage), "" + context.getResources().getString(R.string.ok), "Cancel", "PLAYSTORE", true);
//                }
            }
        });
        if (position == (getItemCount() - 1)) {
            ((SpotPICKUPSActivity) context).hideLoader();
        }
    }

    @Override
    public int getItemCount() {
        if (spotPickUpDos.size() > 0) {
            return spotPickUpDos.size();
        } else {

            return 0;
        }
    }


    private class ProductListAdapter extends BaseAdapter {

        private Context context;
        private ArrayList<ActiveDeliveryDO> activeDeliveoS;

        private ProductListAdapter(Context context, ArrayList<ActiveDeliveryDO> activeDeliveryDOS) {
            this.context = context;
            this.activeDeliveoS = activeDeliveryDOS;
        }

        @Override
        public int getCount() {
            return activeDeliveoS != null ? activeDeliveoS.size() : 0;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View view = LayoutInflater.from(context).inflate(R.layout.shipment_detail_data, null);
            TextView tvProductName = view.findViewById(R.id.tvName);
            TextView tvDescription = view.findViewById(R.id.tvDescription);
            TextView tvNumber = view.findViewById(R.id.tvNumber);
            TextView tvAvailableQty = view.findViewById(R.id.tvAvailableQty);
//            tvProductName.setText(activeDeliveoS.get(position).product);
            tvDescription.setText("" + activeDeliveoS.get(position).productDescription);
            tvNumber.setText("" + activeDeliveoS.get(position).totalQuantity + " " + activeDeliveoS.get(position).unit);
            tvAvailableQty.setVisibility(View.GONE);

            return view;
        }

    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    private void clearDataLocalData() {
        PreferenceUtils preferenceUtils = new PreferenceUtils(context);

        preferenceUtils.removeFromPreference(PreferenceUtils.CHECKIN_DATE);
        preferenceUtils.removeFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID);
        preferenceUtils.removeFromPreference(PreferenceUtils.CUSTOMER);
        preferenceUtils.removeFromPreference(PreferenceUtils.LATTITUDE);
        preferenceUtils.removeFromPreference(PreferenceUtils.LONGITUDE);
        preferenceUtils.removeFromPreference(PreferenceUtils.VEHICLE_CHECKED_ROUTE_ID);
        preferenceUtils.removeFromPreference(PreferenceUtils.CARRIER_ID);
        preferenceUtils.removeFromPreference(PreferenceUtils.NOTES);
        preferenceUtils.removeFromPreference(PreferenceUtils.OPENING_READING);
        preferenceUtils.removeFromPreference(PreferenceUtils.ODO_UNIT);

        preferenceUtils.removeFromPreference(PreferenceUtils.CLOSING_READING);
        preferenceUtils.removeFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID);
        preferenceUtils.removeFromPreference(PreferenceUtils.INVOICE_ID);
        preferenceUtils.removeFromPreference(PreferenceUtils.SHIPMENT);
        preferenceUtils.removeFromPreference(PreferenceUtils.SITE_NAME);
        preferenceUtils.removeFromPreference(PreferenceUtils.VOLUME);
        preferenceUtils.removeFromPreference(PreferenceUtils.WEIGHT);
        preferenceUtils.removeFromPreference(PreferenceUtils.CV_PLATE);
        preferenceUtils.removeFromPreference(PreferenceUtils.CA_DATE);
        preferenceUtils.removeFromPreference(PreferenceUtils.CA_TIME);
        preferenceUtils.removeFromPreference(PreferenceUtils.CD_DATE);
        preferenceUtils.removeFromPreference(PreferenceUtils.CVEHICLE_CODE);
        preferenceUtils.removeFromPreference(PreferenceUtils.CD_TIME);
        preferenceUtils.removeFromPreference(PreferenceUtils.CN_SHIPMENTS);
        preferenceUtils.removeFromPreference(PreferenceUtils.CHECK_IN_STATUS);
        preferenceUtils.removeFromPreference(PreferenceUtils.TOTAL_LOAD);
        preferenceUtils.removeFromPreference(PreferenceUtils.VEHICLE_CAPACITY);
        preferenceUtils.removeFromPreference(PreferenceUtils.SELECTION);
        AppPrefs.clearPref(context);
        StorageManager.getInstance(context).deleteCheckInData(context);
        StorageManager.getInstance(context).deleteCompletedShipments(((TransactionList) context));
        StorageManager.getInstance(context).deleteSkipShipmentList(((TransactionList) context));
        StorageManager.getInstance(context).deleteVanNonScheduleProducts(((TransactionList) context));
        StorageManager.getInstance(context).deleteVanScheduleProducts(((TransactionList) context));
        StorageManager.getInstance(context).deleteSpotSalesCustomerList(((TransactionList) context));
        StorageManager.getInstance(context).deleteVehicleInspectionList(((TransactionList) context));
        StorageManager.getInstance(context).deleteGateInspectionList(((TransactionList) context));
        StorageManager.getInstance(context).deleteSiteListData(((TransactionList) context));
        StorageManager.getInstance(context).deleteShipmentListData(((TransactionList) context));
        StorageManager.getInstance(context).deleteCheckOutData(context);
        StorageManager.getInstance(context).deleteDepartureData(((TransactionList) context));
        StorageManager.getInstance(context).deleteUpdateData(((TransactionList) context));

        StorageManager.getInstance(context).deleteScheduledNonScheduledReturnData();// clearing all tables
//        preferenceUtils.saveString(PreferenceUtils.STATUS, "" + resources.getString(R.string.logged_in))
//        preferenceUtils.saveString(PreferenceUtils.CHECK_IN_STATUS, resources.getString(R.string.logged_in))
        Intent intent = new Intent(context, LoginActivity.class);
        context.startActivity(intent);
    }

}
