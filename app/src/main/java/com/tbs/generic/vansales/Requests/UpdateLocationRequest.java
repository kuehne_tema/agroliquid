package com.tbs.generic.vansales.Requests;

/**
 * Created by Vijay on 19-05-2016.
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.generic.vansales.Model.SuccessDO;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;

public class UpdateLocationRequest extends AsyncTask<String, Void, Boolean> {

    private SuccessDO successDO;
    private Context mContext;

    PreferenceUtils preferenceUtils;
    String customerId,addressCode;
    String telephone1;String email1;String mobile1;String addressLine11;String addressLine21;
    String  addressLine31;String cityName1;String postalcode1;
    int type;
    Double lat,lng;

    public UpdateLocationRequest(String telephone,String email,String mobile,String addressLine1,String addressLine2,
                                 String  addressLine3,String cityName,String postalcode,
                                 String  customerID,String addressCode,int typ, Double lattitude, Double longitude, Context mContext) {

        this.mContext = mContext;
        this.customerId = customerID;
        this.addressCode = addressCode;
        this.type = typ;

        this.lat = lattitude;
        this.lng = longitude;
        this.telephone1 = telephone;
        this.email1 = email;
        this.mobile1 = mobile;
        this.addressLine11 = addressLine1;
        this.addressLine21 = addressLine2;
        this.addressLine31 = addressLine3;
        this.cityName1 = cityName;
        this.postalcode1 = postalcode;


    }


    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, SuccessDO loadStockMainDO);

    }

    public boolean runRequest() {
        preferenceUtils = new PreferenceUtils(mContext);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("I_TYP", type);
            jsonObject.put("I_BPR", customerId);
            jsonObject.put("I_ADDCOD", addressCode);
            jsonObject.put("I_LAT", lat+"");
            jsonObject.put("I_LOG", lng+"");
            jsonObject.put("I_ADDLIN1", addressLine11+"");
            jsonObject.put("I_ADDLIN2", addressLine21);
            jsonObject.put("I_ADDLIN3", addressLine31);
            jsonObject.put("I_ADDLIN3", addressLine31);
            jsonObject.put("I_POSCOD", postalcode1);
            jsonObject.put("I_CTY", cityName1);
            jsonObject.put("I_MOB", mobile1);
            jsonObject.put("I_TEL", telephone1);
            jsonObject.put("I_EMIL", email1);


        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();

        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.UPDATE_LOCATION, jsonObject);

        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }

    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();


            successDO = new SuccessDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {
                        if (attribute.equalsIgnoreCase("O_FLAG")) {
                            if(text.length()>0){

                                successDO.flag = Integer.parseInt(text);
                            }


                        }
                        text="";
                    }

                    if (endTag.equalsIgnoreCase("GRP")) {
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                    }

                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

       // ProgressTask.getInstance().showProgress(mContext, false, "Retrieving Details...");
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

//        ProgressTask.getInstance().closeProgress();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, successDO);
        }
    }
}