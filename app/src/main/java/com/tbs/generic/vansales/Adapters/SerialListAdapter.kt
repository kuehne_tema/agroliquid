package com.tbs.generic.vansales.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.tbs.generic.vansales.Activitys.BaseActivity
import com.tbs.generic.vansales.Model.SerialListDO
import com.tbs.generic.vansales.Model.TrailerSelectionDO
import com.tbs.generic.vansales.R
import java.util.*

/**
 * Created by sandy on 2/7/2018.
 */
class SerialListAdapter(private val context: Context, private val serialDOS: ArrayList<SerialListDO>) : RecyclerView.Adapter<SerialListAdapter.MyViewHolder>() {
    private val type = 1

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tvName: TextView
        var checkbox: CheckBox

        init {
            tvName = view.findViewById(R.id.tvSerials)
            checkbox = view.findViewById(R.id.checkbox)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_stock_selection, parent, false)
        return MyViewHolder(itemView)
    }
    private val selectedDOs = ArrayList<SerialListDO>()
    fun getSelectedSerialListDO(): ArrayList<SerialListDO>? {
        return selectedDOs
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val bankDO = serialDOS[position]
        holder.tvName.text = "" + bankDO.serialDO
        holder.checkbox.setOnCheckedChangeListener { buttonView, isChecked ->
            bankDO.ischecked = isChecked
            if (isChecked) {
                if (selectedDOs.size < 1) {
                    selectedDOs.add(bankDO)
                } else {
                    holder.checkbox.setChecked(false)
                    //                        holder.cbSelected.setEnabled(false);
                    (context as BaseActivity).showToast(context.getResources().getString(R.string.max_1))
                }
            } else {
                selectedDOs.remove(bankDO)
            }
        }

    }

    override fun getItemCount(): Int {
        return serialDOS.size
    }


    fun getSelectedCountCheck(): Boolean {
        var count: Boolean = false
        for (seroDo in serialDOS) {
            if (seroDo.ischecked) {
                count = true;
            }
        }

        return count;
    }
}