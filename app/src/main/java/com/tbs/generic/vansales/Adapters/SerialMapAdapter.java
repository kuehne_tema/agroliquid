package com.tbs.generic.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tbs.generic.vansales.Model.SerialListDO;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.utils.PreferenceUtils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class SerialMapAdapter extends RecyclerView.Adapter<SerialMapAdapter.MyViewHolder> {
    int count = 0;
    private LinkedHashMap<String, ArrayList<SerialListDO>> orderRequestsDosMap;
    private ArrayList<SerialListDO> availableDOS;
    private Context context;
    private int type;
    private PreferenceUtils preferenceUtils;

    public void refreshAdapter(Context context, @NotNull LinkedHashMap<String, ArrayList<SerialListDO>> orderRequestsDosMap, ArrayList<SerialListDO> availableDos) {
        this.orderRequestsDosMap.clear();
        this.availableDOS.clear();
        this.orderRequestsDosMap = orderRequestsDosMap;
        this.availableDOS.addAll(availableDos);
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvEquioment;
        private RecyclerView rvList;

        public MyViewHolder(View view) {
            super(view);
            tvEquioment = view.findViewById(R.id.tvEquioment);
            rvList = view.findViewById(R.id.rvList);


        }
    }


    public SerialMapAdapter(Context context, LinkedHashMap<String, ArrayList<SerialListDO>> orderRequestsDosMap, ArrayList<SerialListDO> availableDos) {
        this.context = context;
        this.orderRequestsDosMap = orderRequestsDosMap;
        this.availableDOS = availableDos;
        preferenceUtils = new PreferenceUtils(context);
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.vanstock_serial, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        ArrayList<String> keySet = new ArrayList<>(orderRequestsDosMap.keySet());
        if ((position % 2 == 0)) {
            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.light_grey));
        } else {
            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.light_secnd_grey));
        }
        ArrayList<SerialListDO> orderRequestsDos = orderRequestsDosMap.get(keySet.get(position));
        if (availableDOS != null && availableDOS.size() > 0) {
            final SerialListDO orderRequestsDO = availableDOS.get(position);

            holder.tvEquioment.setText("" + orderRequestsDO.equipmentCode + " - " + orderRequestsDO.equipmentDes);


            holder.rvList.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
            holder.rvList.setAdapter(new ReturnProductsListAdapter(context, orderRequestsDos));

        }
    }

    @Override
    public int getItemCount() {
        return orderRequestsDosMap != null ? orderRequestsDosMap.keySet().size() : 0;
    }

    private class ReturnProductsListAdapter extends RecyclerView.Adapter<ReturnListHolder> {
        private ArrayList<SerialListDO> orderRequestsDos;
        private Context context;

        ReturnProductsListAdapter(Context context, ArrayList<SerialListDO> orderRequestsDos) {
            this.context = context;
            this.orderRequestsDos = orderRequestsDos;
        }

        @NonNull
        @Override
        public ReturnListHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
            View itemView = LayoutInflater.from(context).inflate(R.layout.item_seria, viewGroup, false);
            return new ReturnListHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull ReturnListHolder holder, int position) {
            final SerialListDO orderRequestsDO = orderRequestsDos.get(position);
            holder.tvSerials.setText(orderRequestsDO.serialDO);
        }

        @Override
        public int getItemCount() {
            return orderRequestsDos != null ? orderRequestsDos.size() : 0;
        }
    }

    private class ReturnListHolder extends RecyclerView.ViewHolder {
        private TextView tvSerials;

        public ReturnListHolder(@NonNull View itemView) {
            super(itemView);
            tvSerials = itemView.findViewById(R.id.tvSerials);


        }
    }
}
