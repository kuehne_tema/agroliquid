package com.tbs.generic.vansales.Activitys

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import android.widget.*
import com.tbs.generic.vansales.Adapters.DamageScheduledProductsAdapter
import com.tbs.generic.vansales.Model.ActiveDeliveryDO
import com.tbs.generic.vansales.Model.LoadStockDO
import com.tbs.generic.vansales.Model.LoanReturnDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.*
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import kotlin.collections.ArrayList
import kotlin.collections.LinkedHashMap

//
class SelectedDamageBGActivity : BaseActivity() {
    lateinit var invoiceAdapter: DamageScheduledProductsAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>
    lateinit var recycleview : androidx.recyclerview.widget.RecyclerView
    lateinit var tvNoDataFound:TextView
    lateinit var tvBalanceQty:TextView
    lateinit var tvReturnedQty:TextView
    lateinit var tvIssuedQty:TextView
    lateinit var tvOpeningQty:TextView
    lateinit var openingQty:String
    private var issuedQty : Int = 0
    private var type : Int = 1

    private var   returnQty :Double=0.0
    private var balanceQty:Double=0.0
    private lateinit var btnConfirm: Button
    private lateinit var rbEmptyCylinder: RadioButton
    private lateinit var rbSalesReturn: RadioButton
    private var loanReturnDOsMap: LinkedHashMap<String, ArrayList<LoanReturnDO>> = LinkedHashMap()
    private var loanReturnDos: ArrayList<ActiveDeliveryDO> = ArrayList()

    override fun initialize() {
      val  llCategories = layoutInflater.inflate(R.layout.selected_nonbg, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
//        ivAdd.setVisibility(View.VISIBLE)

        if (intent.hasExtra("Products")) {
            loanReturnDos = intent.getSerializableExtra("Products") as ArrayList<ActiveDeliveryDO>
        }


        initializeControls()
        invoiceAdapter = DamageScheduledProductsAdapter(this@SelectedDamageBGActivity, loanReturnDos,"","")
        recycleview.adapter = invoiceAdapter


        btnConfirm.setOnClickListener {
            Util.preventTwoClick(it)
            showAppCompatAlert("", getString(R.string.you_cant_modify_this), getString(R.string.ok), getString(R.string.cancel), getString(R.string.success), true)

        }


    }




    override fun initializeControls() {
        tvScreenTitle.text = getString(R.string.selected_damaged_cylinders)
        recycleview = findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.recycleview)
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
        tvNoDataFound = findViewById<TextView>(R.id.tvNoDataFound)
        tvOpeningQty = findViewById<TextView>(R.id.tvOpeningQty)
        tvIssuedQty = findViewById<TextView>(R.id.tvIssuedQty)
        tvReturnedQty = findViewById<TextView>(R.id.tvReturnedQty)
        tvBalanceQty = findViewById<TextView>(R.id.tvBalanceQty)
        recycleview.layoutManager = linearLayoutManager
        btnConfirm = findViewById<Button>(R.id.btnConfirm)


    }

    override fun onButtonYesClick(from: String) {

        if (getString(R.string.success).equals(from, ignoreCase = true)) {
            updateDamageCylinders()
        } else
            if (getString(R.string.failure).equals(from, ignoreCase = true)) {


                //finish()
            }

    }
    private fun updateDamageCylinders() {
      var  shipmentId = preferenceUtils.getStringFromPreference(PreferenceUtils.SHIPMENT, "")

        val request = UpdateDamageCylindersRequest(shipmentId, loanReturnDos, this@SelectedDamageBGActivity)
        request.setOnResultListener { isError, approveDO ->
            hideLoader()
            if (approveDO != null) {
                if (isError) {
                    showAppCompatAlert("", getString(R.string.please_try_again), getString(R.string.ok), getString(R.string.cancel), "", false)

                } else {
                    if (approveDO.flag == 1) {
                        showToast(getString(R.string.updated_successfully))
                        setResult(98, intent)
                        finish()
                    } else {
                        showAppCompatAlert("", getString(R.string.please_try_again), getString(R.string.ok), getString(R.string.cancel), "", false)

                    }
                }
            }
        }
        request.execute()
    }



}