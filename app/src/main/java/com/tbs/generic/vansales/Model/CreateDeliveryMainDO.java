package com.tbs.generic.vansales.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class CreateDeliveryMainDO implements Serializable {

    public String customerId = "";
    public String site = "";
    public String deliveryNumber = "";
    public String message = "";
    public int status = 0;
    public int productGroupType = 0;
    public String recieptNumber = "";
    public int flag = 0;

    public ArrayList<CreateDeliveryDO> createDeliveryDOS = new ArrayList<>();


}
