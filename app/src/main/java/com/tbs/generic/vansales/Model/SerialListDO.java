package com.tbs.generic.vansales.Model;

import java.io.Serializable;

public class SerialListDO implements Serializable {

    public int flag = 0;
    public String serialDO = "";
    public String version = "";
    public boolean ischecked;
    public String location = "";
    public int state = 0;

    public String equipmentCode = "";
    public String equipmentDes = "";

}
