package com.tbs.generic.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tbs.generic.vansales.Activitys.AddressListActivity;
import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Activitys.CustomerDetailsActivity;
import com.tbs.generic.vansales.Model.CustomerDo;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.common.AppConstants;
import com.tbs.generic.vansales.utils.PreferenceUtils;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class SpotSalesCustomerAdapter extends RecyclerView.Adapter<SpotSalesCustomerAdapter.MyViewHolder> {

    private ArrayList<CustomerDo> customerDos;
    private Context context;
    private String from;
    int type;


    public void refreshAdapter(@NotNull ArrayList<CustomerDo> customerDos) {
        this.customerDos = customerDos;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvCustomerName, tvCustomerCode, tvBuisinessLine, tvAddressCode, tvDescription, tvCity, tvCountry;
        public TextView btnPay;
        private LinearLayout llDetails;
        public TextView tvOrderId;

        public MyViewHolder(View view) {
            super(view);
            llDetails = view.findViewById(R.id.llDetails);
            tvCustomerName = itemView.findViewById(R.id.tvCustomerName);
            tvCustomerCode = itemView.findViewById(R.id.tvCustomerCode);
            tvCity = itemView.findViewById(R.id.tvCity);
            tvCountry = itemView.findViewById(R.id.tvCountry);


        }
    }


    public SpotSalesCustomerAdapter(Context context, ArrayList<CustomerDo> listOrderDos, String froM,int type) {
        this.context = context;
        this.customerDos = listOrderDos;
        this.from = froM;
        this.type = type;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.spotsales_customer_data, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final CustomerDo customerDo = customerDos.get(position);
        customerDo.isCreditAvailable = customerDo.flag == 1;
        holder.tvCustomerCode.setText(customerDo.customer);
        holder.tvCustomerName.setText(customerDo.customerName);
        holder.tvCity.setText(customerDo.city);
        holder.tvCountry.setText(customerDo.countryName);

        if (customerDo.isDelivered != null && customerDo.isDelivered.equalsIgnoreCase("Delivered")) {
            holder.llDetails.setBackgroundColor(context.getResources().getColor(R.color.gray_new));
            holder.llDetails.setClickable(false);
            holder.llDetails.setEnabled(false);
        } else {
            holder.llDetails.setClickable(true);
            holder.llDetails.setEnabled(true);
        }
        holder.llDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!from.isEmpty() && from.equalsIgnoreCase("MASTER")) {
//                    if (StorageManager.getInstance(context).saveCurrentSpotSalesCustomer(((BaseActivity) context), customerDo)) {
                    if(type==1){
                        Intent intent = new Intent(context, CustomerDetailsActivity.class);
                        intent.putExtra("C_CODE", customerDo.customer);
                        intent.putExtra("MASTER", "MASTER");
                        intent.putExtra("TYPE", type);
                        intent.putExtra("CUSTOMER_DO", customerDo);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        context.startActivity(intent);
                    }else {
                        Intent intent = new Intent(context, AddressListActivity.class);
                        intent.putExtra("CODE", customerDo.customer);
                        intent.putExtra("MASTER", "MASTER");
                        intent.putExtra("TYPE", type);
                        intent.putExtra("CUSTOMER_DO", customerDo);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        context.startActivity(intent);
                    }


                } else {

                    Intent intent = new Intent(context, AddressListActivity.class);
                    intent.putExtra("CODE", customerDo.customer);
                    intent.putExtra("CUSTOMER_DO", customerDo);
                    ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.CURRENCY, "" +customerDo.currency);

                    ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.ProductsType, AppConstants.FixedQuantityProduct);
                    ((BaseActivity) context).preferenceUtils.saveString(PreferenceUtils.ShipmentType, context.getResources().getString(R.string.checkin_non_scheduled));
                    context.startActivity(intent);
                }

            }
        });


    }

    @Override
    public int getItemCount() {

        return customerDos != null ? customerDos.size() : 0;

    }

}
