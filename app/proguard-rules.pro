# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile



-dontnote com.itextpdf.**
-keep class javax.** { *; }
-keep class org** { *; }
-dontnote com.github.bumptech.**


-dontnote android.**
-dontnote java.**
-dontnote javax.**
-dontnote junit.**
-dontnote org.**
-dontnote dalvik.**
-dontnote com.android.internal.**

-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.preference.Preference

-ignorewarnings

-keep class com.tbs.generic.vansales.Model** { *; }

-keepnames class * implements java.io.Serializable
-keep class javax.mail.** {*;}
-keep public class com.tbs.generic.vansales.pdfs.utils.PDFConstants.** { *; }
-keep public class com.tbs.generic.vansales.pdfs.utils.** { *; }
-keep public class com.tbs.generic.vansales.mail.GmailSender.** { *; }
-keep public class com.tbs.generic.vansales.mail.TBSMailBG.** { *; }
-keep public class com.tbs.generic.vansales.mail.JSSEProvider.** { *; }

-keep class com.sun.mail.** {*;}